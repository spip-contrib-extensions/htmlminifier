<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// H
	'htmlminifier_description' => 'Minifier les pages HTML servies par SPIP.',
	'htmlminifier_nom' => 'HTML Minifier',
	'htmlminifier_slogan' => '',
);
