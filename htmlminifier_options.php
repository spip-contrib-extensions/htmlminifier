<?php
/**
 * Options au chargement du plugin HTML Minifier
 *
 * @plugin     HTML Minifier
 * @copyright  2018
 * @author     ladnet
 * @licence    GNU/GPL
 * @package    SPIP\Htmlminifier\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('class/HTMLMinifier');