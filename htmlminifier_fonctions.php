<?php
/**
 * Fonctions utiles au plugin HTML Minifier
 *
 * @plugin     HTML Minifier
 * @copyright  2017
 * @author     ladnet
 * @licence    GNU/GPL
 * @package    SPIP\Htmlminifier\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}